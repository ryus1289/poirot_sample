var http = require('http');
var fs1 = require('fs');
var fs2 = require('fs');
var server = http.createServer(getXml);
var encode_data;

// サーバを待ち受け状態にする
server.listen(3000);
console.log('Server Start');

// 対象ファイルをbase64でエンコード
fs1.readFile('encode.xml', 'base64', function(err, data) {
    if (err) throw err;
    encode_data = data;  
});

//server.on('request', function(req, res) {
function getXml(req, res) {
    fs2.readFile('sample.xml', 'UTF-8', 
        (error, data)=>{
            var content = data.replace(/encode_data/g, encode_data);
            res.writeHead(200, {'Content-Type' : 'text/plain'});
            res.write(content);
            res.end();
        }
    );
}
