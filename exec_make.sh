#!/bin/sh

NOUPDATE="Already"

#1. git管理対象まで移動
cd ~/works/poirot_sample/vm2

#2. ブランチの移動
git checkout master

#3. git pullの実行結果を配列に格納
array=(`git pull origin master`)

#4. git pullの実行結果を配列に格納
#  更新があればtarで固めた後、各サーバに転送
if [ ${array[0]} = $NOUPDATE ]; then
    echo "pullによる更新なし" 
else
    echo "pullによる更新あり" 
    #make clean / tar / 配布
    tar cvfz `date "+%Y%m%d%H%M%S"_bin.tar.gz` *
fi


