#!/bin/sh
#
# Author:   T.K
# License:  MIT License
# Created:  2018-11-18
#
###############################################################################################
# バイナリアップデートツール
#
#   [仕様]
#     所定の場所に格納されている最新バイナリを自環境に展開、プロセスを再起動する
#
#   [使い方]
#     update_bin.sh
###############################################################################################

# 実行ディレクトリ
SCRIPT_DIR=$(cd $(dirname $0); pwd)

# tmpファイル
SCRIPT=${SCRIPT_DIR}/script.sh
FILELIST=${SCRIPT_DIR}/filelist

# バイナリが存在しているサーバ
SERVER=agentd

# アカウント(おそらく使用しない)
USER=""

# バイナリ格納ディレクトリ
DIR=/home/vagrant/release

# SCP取得ファイル名
SCP_FILE=updatefile.tar.gz

# バイナリ情報
# 0:agentd
# 1:trmmngd
# 2:tpm
# バイナリアップデート先のディレクトリ
FUNC_PATH=(
"/Users/Tatsuya/git/poirot_sample/bin"
)
# 比較情報を表示するバイナリ名
FUNC_BIN=(
agentd
)

# 再起動スクリプト
WITCH_DIR=/root/witch.sh

###############################################################################################

function usage() {
    echo ""
    echo "Usage: $0"
    echo ""
}

function yesno() {
    read -p "$1 (Y/n) >> " answer
    answer=`printf $answer | grep -iE '^(yes|y|no|n)$' | tr "[:lower:]" "[:upper:]"`
    case ${answer} in
        "Y" | "YES") return 0;;
        "N" | "NO") return 1;;
        *) yesno;;
    esac
}

function tmpdelete() {
    if [ -e ${SCRIPT} ]; then
        rm ${SCRIPT}
    fi
    if [ -e ${FILELIST} ]; then
        rm ${FILELIST}
    fi
    if [ -e /tmp/${SCP_FILE} ]; then
        rm /tmp/${SCP_FILE}
    fi
}

if [ $# -gt 0 ]; then
    usage
    exit 1
fi

# 対象のファイルリストを取得
echo '
    ls -al '${DIR}'/*
' > ${SCRIPT}
ssh -q ${USER}${SERVER} 'bash -s' < ${SCRIPT} > ${FILELIST}
LIST=($(cat ${FILELIST} | grep tar.gz | awk '{print $9}'));

echo "--------------------------------------------------------------------------"
echo " アップデートするバイナリを選択してください"
echo "--------------------------------------------------------------------------"
COUNT=1
for file in ${LIST[*]}; do
    echo "  "${COUNT}：${file}
    COUNT=$((++COUNT))
done
echo "--------------------------------------------------------------------------"
read -p "Input Number >> " UPDATE
UPDATE=$((--UPDATE))

echo "--------------------------------------------------------------------------"
echo " アップデートする機能を選択してください"
echo "--------------------------------------------------------------------------"
echo "  "1：agentd
echo "  "2：trmmngd
echo "  "3：tpm TODO
echo "--------------------------------------------------------------------------"
read -p "Input Number >> " FUNC
FUNC=$((--FUNC))

# 対象のファイル情報を取得
INFO_FILE=${LIST[${UPDATE}]}
INFO_BIN=${FUNC_BIN[${FUNC}]}

echo '
    echo "--------------------------------------------------------------------------"
    echo " バイナリ確認"
    echo "--------------------------------------------------------------------------"
    less '${INFO_FILE}' | grep '${INFO_BIN}'
' > ${SCRIPT}

ssh -q ${USER}${SERVER} 'bash -s' < ${SCRIPT}
echo " ↓↓↓"
ls -al ${FUNC_PATH[${FUNC}]}/${INFO_BIN}
echo "--------------------------------------------------------------------------"

yesno "アップデートを開始します"
if [ $? -eq 1 ]; then
    echo "アップデートを中止します"
    tmpdelete
    exit 1
fi

# 対象ファイルの取得
scp ${USER}${SERVER}:${INFO_FILE} /tmp/${SCP_FILE}

# バイナリを展開
#tar xvfz /tmp/${INFO_FILE} --directory=${FUNC_PATH[${FUNC}]}

# 再起動
#sh ${WITCH_DIR} restart all

tmpdelete
exit 0
